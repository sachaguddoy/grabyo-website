<?php
/*
Template Name: Home
*/
?>

<?php get_header(); ?>

<?php az_page_header($post->ID); ?>

<div id="content">

	<div id="landing">
		<div class="landing-wrapper">
			<div class="container landing-content">
				<h1>Cloud-based video production, editing and distribution</h1>
					<br>
				<h2>Trusted by the world's leading rightsholders and broadcasters.</h2>
				<p>
					<a class="btn-cta" href="/try-grabyo/">
						Try Grabyo
						<i class="font-icon-arrow-right-simple-thin-round"></i>
					</a>
				</p>

			</div>
			<div class="hide-overflow">
			<video autoplay loop controls muted poster="/wp-content/themes/grabyo-chariot/video/back.jpg" id="bgvid">
				<source src="/wp-content/themes/grabyo-chariot/video/home.mp4" type="video/mp4">
				<source src="/wp-content/themes/grabyo-chariot/video/home.webm" type="video/webm">
				<source src="/wp-content/themes/grabyo-chariot/video/home.ogv" type="video/ogg">
			</video>
		</div>
		</div>

	</div>



	<script>

		(function($){
			$(document).ready(function(){

				var $landing = $('#landing'),
				$window = $(window);

				var setLandingHeight = function() {
  				$landing.height($window.innerHeight() - $('.normal-header').height());
				}
				$window.resize(setLandingHeight);
				$window.on('orientationchange', setLandingHeight);
				setLandingHeight();

			});
		})(jQuery);
	</script>

	<?php /* Now come the content */ ?>

	<section class="content-wrapper">
		<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
	        <?php //edit_post_link( __('Edit', AZ_THEME_NAME), '<span class="edit-post">[', ']</span>' ); ?>
	        <?php the_content(); ?>
	        <?php wp_link_pages(array('before' => '<p><strong>'.__('Pages:', 'zilla').'</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
	    <?php endwhile; endif; ?>
	</section>
</div>


<?php get_footer(); ?>
<!-- Slider mac -->
<script type="text/javascript" src="/wp-content/themes/grabyo-chariot/js/slider-home.js"></script>
<!-- Slider mac end -->
