(function($) {

    var $phone = $('.js-phone');
    var PHONE_ACTIVE_CLASS = 'active';

    var showActivePhone = function(index) {
      $('.js-phone[data-slide-index="' + index + '"]').addClass(PHONE_ACTIVE_CLASS);
    };

    $('.home-screen-sliders ul').bxSlider({
      mode: 'fade',
      captions: false,
      speed: 500,
      randomStart: false,
      pager: false,
      controls: false,
      auto: true,
      pause: 6000,
      minSlides: 2,
      onSlideBefore: function($slider, oldIndex, newIndex) {
          $('.js-phone').removeClass(PHONE_ACTIVE_CLASS);
          showActivePhone(newIndex);
      },
      onSliderLoad: function(currentIndex) {
        showActivePhone(currentIndex);
      }
    });

    $('.client-logo-slider').bxSlider({
        minSlides: 4,
        slideWidth: 150,
        slideMargin: 10,
        ticker: true,
        speed: 80000
    });

})(jQuery);
