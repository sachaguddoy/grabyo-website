(function($) {

	var CONSTS = {
		active : 'active'
	};

	$(document).ready(function(){

		// var $categoryTitles = $('.faq-category-item-title');
		var $subCategoryTitles = $('.faq-article-title');
		var $toggleAllLinks = $('.toggle-all');

		$subCategoryTitles.click(function() {

			var $target = $(this).parent();
			$target.toggleClass(CONSTS.active);
		});

		$toggleAllLinks.click(function() {

			var $this = $(this);

			if ($(this).parent().next().hasClass('faq-subcategory-item-title')) {

				// With subcategories

				if ($this.data('status') === 'collapsed') {
					
					$(this).parent().siblings('.faq-articles-list').find('.faq-article').addClass(CONSTS.active);					
					$this.data('status', 'expanded');
					$this.text("[ Collapse all ]");
				} else {

					$(this).parent().siblings('.faq-articles-list').find('.faq-article').removeClass(CONSTS.active);
					$this.data('status', 'collapsed');
					$this.text("[ Expand all ]");
				}


			} else {

				// Without subcategories

				if ($this.data('status') === 'collapsed') {
					$(this).parent().next().find('.faq-article').addClass(CONSTS.active);
					$this.data('status', 'expanded');
					$this.text("[ Collapse all ]");
				} else {
					$(this).parent().next().find('.faq-article').removeClass(CONSTS.active);
					$this.data('status', 'collapsed');
					$this.text("[ Expand all ]");
				}
			}

		});

	});
})(jQuery);