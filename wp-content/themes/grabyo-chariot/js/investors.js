(function($) {

	$(document).ready(function(){

		var $window = $(window);
		var $slide;

		var generateOption = function(index) {
		    var width = $window.width();
		    return {
		        adaptiveHeight : true,
		        responsive : false,
		        infiniteLoop : false,
		        controls: false,
		        startSlide: index === undefined ? 0 : index
		    }
		}


		var onWindowResize = function() { 
		    if ($window.width() > 768) {
		    	if ($slide !== undefined) {
		    		$slide.destroySlider();
		    	}
		    } else {
			    var index = $slide !== undefined ? $slide.getCurrentSlide() : 0;
			    var option = generateOption(index);

			    if ($slide === undefined) {
					$slide = $('.slider').bxSlider(generateOption());
			    } else {
			    	$slide.reloadSlider(option);
			    }

		    	
		    }
		}

		$window.resize(onWindowResize);
		onWindowResize();
	});

})(jQuery);