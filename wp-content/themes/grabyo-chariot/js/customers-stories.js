(function($){
	$(document).ready(function(){
		var slider = $('.customer-stories').bxSlider({
			randomStart: true,
			controls: false,
			auto: true,
			pause: 6000,
			autoHover: true
		});
	});
})(jQuery);