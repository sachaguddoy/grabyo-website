$ = jQuery;
$(document).on('ready', function() {
    $('.js-download-link').on('click', function(e) {
        ga('send', 'event',  {
            eventCategory: 'Download Link',
            eventAction: 'click',
            eventLabel: getUrlQueryValue('user_id', window.location.href),
            transport: 'beacon'
        });
    });
    
    function getUrlQueryValue( name, url ) {
        if (!url) url = location.href;
        name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
        var regexS = "[\\?&]"+name+"=([^&#]*)";
        var regex = new RegExp( regexS );
        var results = regex.exec( url );
        return results == null ? null : results[1];
    }
});

