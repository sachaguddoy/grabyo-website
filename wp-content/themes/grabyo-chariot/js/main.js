$(document).on('ready', function() {
    if($('.client-logo-slider').length > 0) {
        $('.client-logo-slider').bxSlider({
            minSlides: 4,
            slideWidth: 150,
            slideMargin: 10,
            ticker: true,
            speed: 80000
        });
    }
    
    var onVideoStartClick = function() {
        $investorsVideo[0].play();
        
        $investorsVideo.one('ended', onVideoEnded);

        var _this = this;

        $('.investor-player-image .img-caption').css({
            'top' : '-20px',
            'opacity' : 0
        });

        setTimeout(function() {
            $('.investors-video').delay(500).css('display', 'block');
            $('.investor-player-image').delay(500).css('display', 'none');
            $('.investor-player').addClass('playing');
            $('#who-is-involved').addClass('video-display');
        }, 500);
    };

    var onVideoEnded = function() {
        $('.investor-player-image .img-caption').css({
            'top' : 'auto',
            'opacity' : 1
        });			
        $('#who-is-involved').removeClass('video-display');
        $('.investor-player').removeClass('playing');
        $('.investors-video').css('display', 'none');
        $('.investor-player-image').css('display', 'block');
    };
    
    if($('#investor-video-displayer').length > 0) {
        var $investorsVideo = $('#investors-video');
        $('#investor-video-displayer').click(onVideoStartClick);
    }
});