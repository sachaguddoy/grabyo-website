<?php $options = get_option('chariot'); ?>
</div>
<!-- End Main -->

<footer>
<?php if ( is_home() || is_search() || is_404() ) {
	// Blog Page and Search Page
	az_footer_widget(get_option('page_for_posts'));
}
else {
	// All Other Pages and Posts
	az_footer_widget(get_the_ID());
}
?>

<!-- Start Footer Credits -->
<section id="footer-credits">
	<div class="container">
		<div class="row">

			<?php if( !empty($options['enable-footer-social-area']) && $options['enable-footer-social-area'] == 1) { ?>

            <div class="col-md-6">
                <p>Made in London © <?php echo date("Y"); ?> Grabyo Limited </p>
            </div>

            <div class="col-md-6">
            <!-- Social Icons -->
            <nav id="social-footer">
                <ul>
                    <?php
                        global $socials_profiles;

                        foreach($socials_profiles as $social_profile):
                            if( $options[$social_profile.'-url'] )
                            {
                                echo '<li><a href="'.$options[$social_profile.'-url'].'" target="_blank"><i class="font-icon-social-'.$social_profile.'"></i></a></li>';
                            }
                        endforeach;
                    ?>
                </ul>
            </nav>
            <!-- Social Icons -->
            </div>
            <?php } else { ?>

            <div class="col-md-12 textaligncenter">
                <p><p>Made in London © <?php echo date("Y"); ?> Grabyo Limited </p>
            </div>

            <?php } ?>

		</div>
	</div>
</section>
<!-- Start Footer Credits -->

</footer>

</div>
<!-- End Wrap All -->

<?php if( !empty($options['enable-animation']) && $options['enable-animation'] == 1) { ?>
<!-- This section is for Splash Screen -->
<section id="jSplash">
    <div class="loader">
      <div class="circle"></div>
      <div class="circle-inverse"></div>
    </div>
</section>
<!-- End of Splash Screen -->
<?php } ?>


<?php if( !empty($options['enable-back-to-top']) && $options['enable-back-to-top'] == 1) { ?>
<!-- Back To Top -->
<a id="back-to-top" href="#">
	<i class="font-icon-arrow-up-simple-thin-round"></i>
</a>
<!-- End Back to Top -->
<?php } ?>

<?php if(!empty($options['tracking-code'])) echo $options['tracking-code']; ?>

<?php
	wp_register_script('chariot_main', get_stylesheet_directory_uri() . '/_include/js/main.js', array('jquery'), NULL, true);
	wp_enqueue_script('chariot_main');

	wp_localize_script(
		'main',
		'theme_objects',
		array(
			'base' => get_template_directory_uri()
		)
	);

	wp_footer();
?>

<!-- Live Chat -->

<script type='text/javascript'>var fc_CSS=document.createElement('link');fc_CSS.setAttribute('rel','stylesheet');var fc_isSecured = (window.location && window.location.protocol == 'https:');var fc_lang = document.getElementsByTagName('html')[0].getAttribute('lang'); var fc_rtlLanguages = ['ar','he']; var fc_rtlSuffix = (fc_rtlLanguages.indexOf(fc_lang) >= 0) ? '-rtl' : '';fc_CSS.setAttribute('type','text/css');fc_CSS.setAttribute('href',((fc_isSecured)? 'https://d36mpcpuzc4ztk.cloudfront.net':'http://assets1.chat.freshdesk.com')+'/css/visitor'+fc_rtlSuffix+'.css');document.getElementsByTagName('head')[0].appendChild(fc_CSS);var fc_JS=document.createElement('script'); fc_JS.type='text/javascript'; fc_JS.defer=true;fc_JS.src=((fc_isSecured)?'https://d36mpcpuzc4ztk.cloudfront.net':'http://assets.chat.freshdesk.com')+'/js/visitor.js';(document.body?document.body:document.getElementsByTagName('head')[0]).appendChild(fc_JS);window.livechat_setting= 'eyJ3aWRnZXRfc2l0ZV91cmwiOiJncmFieW8uZnJlc2hkZXNrLmNvbSIsInByb2R1Y3RfaWQiOm51bGwsIm5hbWUiOiJHcmFieW8iLCJ3aWRnZXRfZXh0ZXJuYWxfaWQiOm51bGwsIndpZGdldF9pZCI6ImE1NTlhN2VmLTdlZDUtNDMzNC1hNTdmLTFiYzA5YjJlMzQwNyIsInNob3dfb25fcG9ydGFsIjp0cnVlLCJwb3J0YWxfbG9naW5fcmVxdWlyZWQiOmZhbHNlLCJsYW5ndWFnZSI6bnVsbCwidGltZXpvbmUiOm51bGwsImlkIjo1MDAwMDQ3NjIxLCJtYWluX3dpZGdldCI6MSwiZmNfaWQiOiIzMDgxZmUxMzA0NzI2ZTQ3MjE3ZDQ5OTNhMTBhNGYyNSIsInNob3ciOjEsInJlcXVpcmVkIjoyLCJoZWxwZGVza25hbWUiOiJHcmFieW8iLCJuYW1lX2xhYmVsIjoiTmFtZSIsIm1lc3NhZ2VfbGFiZWwiOiJNZXNzYWdlIiwicGhvbmVfbGFiZWwiOiJQaG9uZSIsInRleHRmaWVsZF9sYWJlbCI6IlRleHRmaWVsZCIsImRyb3Bkb3duX2xhYmVsIjoiRHJvcGRvd24iLCJ3ZWJ1cmwiOiJncmFieW8uZnJlc2hkZXNrLmNvbSIsIm5vZGV1cmwiOiJjaGF0LmZyZXNoZGVzay5jb20iLCJkZWJ1ZyI6MSwibWUiOiJNZSIsImV4cGlyeSI6MCwiZW52aXJvbm1lbnQiOiJwcm9kdWN0aW9uIiwiZW5kX2NoYXRfdGhhbmtfbXNnIjoiVGhhbmsgeW91ISEhIiwiZW5kX2NoYXRfZW5kX3RpdGxlIjoiRW5kIiwiZW5kX2NoYXRfY2FuY2VsX3RpdGxlIjoiQ2FuY2VsIiwic2l0ZV9pZCI6IjMwODFmZTEzMDQ3MjZlNDcyMTdkNDk5M2ExMGE0ZjI1IiwiYWN0aXZlIjoxLCJyb3V0aW5nIjp7ImNob2ljZXMiOnsiU2FsZXMiOlsiMCJdLCJNYXJrZXRpbmcgJiBQUiI6WyIwIl0sIlN1cHBvcnQiOlsiMCJdLCJHZW5lcmFsIGVucXVpcmllcyI6WyIwIl0sImRlZmF1bHQiOlsiNTAwMDEzNjA1MCJdfSwiZHJvcGRvd25fYmFzZWQiOiJmYWxzZSJ9LCJwcmVjaGF0X2Zvcm0iOjEsImJ1c2luZXNzX2NhbGVuZGFyIjp7ImJ1c2luZXNzX3RpbWVfZGF0YSI6eyJ3ZWVrZGF5cyI6WzEsMiwzLDQsNV0sIndvcmtpbmdfaG91cnMiOnsiMSI6eyJiZWdpbm5pbmdfb2Zfd29ya2RheSI6Ijk6MDAgYW0iLCJlbmRfb2Zfd29ya2RheSI6IjU6MzAgcG0ifSwiMiI6eyJiZWdpbm5pbmdfb2Zfd29ya2RheSI6Ijk6MDAgYW0iLCJlbmRfb2Zfd29ya2RheSI6IjU6MzAgcG0ifSwiMyI6eyJiZWdpbm5pbmdfb2Zfd29ya2RheSI6Ijk6MDAgYW0iLCJlbmRfb2Zfd29ya2RheSI6IjU6MzAgcG0ifSwiNCI6eyJiZWdpbm5pbmdfb2Zfd29ya2RheSI6Ijk6MDAgYW0iLCJlbmRfb2Zfd29ya2RheSI6IjU6MzAgcG0ifSwiNSI6eyJiZWdpbm5pbmdfb2Zfd29ya2RheSI6Ijk6MDAgYW0iLCJlbmRfb2Zfd29ya2RheSI6IjU6MzAgcG0ifX0sImZ1bGx3ZWVrIjpmYWxzZX0sImhvbGlkYXlfZGF0YSI6W1siSmFuIDEiLCJOZXcgWWVhcidzIERheSJdLFsiQXByIDE4IiwiR29vZCBGcmlkYXkiXSxbIkFwciAyMCIsIkVhc3RlciBTdW5kYXkiXSxbIkFwciAyMSIsIkVhc3RlciBNb25kYXkiXSxbIk1heSA1IiwiRWFybHkgTWF5IEJhbmsgSG9saWRheSJdLFsiTWF5IDI2IiwiU3ByaW5nIEJhbmsgSG9saWRheSJdLFsiQXVnIDI1IiwiU3VtbWVyIEJhbmsgSG9saWRheSJdLFsiRGVjIDI1IiwiQ2hyaXN0bWFzIERheSJdLFsiRGVjIDI2IiwiQm94aW5nIERheSJdXSwidGltZV96b25lIjoiTG9uZG9uIn0sInByb2FjdGl2ZV9jaGF0IjowLCJwcm9hY3RpdmVfdGltZSI6MTUsInNpdGVfdXJsIjoiZ3JhYnlvLmZyZXNoZGVzay5jb20iLCJleHRlcm5hbF9pZCI6bnVsbCwiZGVsZXRlZCI6MCwibW9iaWxlIjoxLCJhY2NvdW50X2lkIjpudWxsLCJjcmVhdGVkX2F0IjoiMjAxNS0xMi0wN1QxNDoyODo0NS4wMDBaIiwidXBkYXRlZF9hdCI6IjIwMTYtMDgtMjVUMTQ6MTU6MjQuMDAwWiIsImNiRGVmYXVsdE1lc3NhZ2VzIjp7ImNvYnJvd3Npbmdfc3RhcnRfbXNnIjoiWW91ciBzY3JlZW5zaGFyZSBzZXNzaW9uIGhhcyBzdGFydGVkIiwiY29icm93c2luZ19zdG9wX21zZyI6IllvdXIgc2NyZWVuc2hhcmluZyBzZXNzaW9uIGhhcyBlbmRlZCIsImNvYnJvd3NpbmdfZGVueV9tc2ciOiJZb3VyIHJlcXVlc3Qgd2FzIGRlY2xpbmVkIiwiY29icm93c2luZ192aWV3aW5nX3NjcmVlbiI6IllvdSBhcmUgdmlld2luZyB0aGUgdmlzaXRvcuKAmXMgc2NyZWVuIiwiY29icm93c2luZ19jb250cm9sbGluZ19zY3JlZW4iOiJZb3UgYXJlIGNvbnRyb2xsaW5nIHRoZSB2aXNpdG9y4oCZcyBzY3JlZW4iLCJjb2Jyb3dzaW5nX3JlcXVlc3RfY29udHJvbCI6IlJlcXVlc3QgdmlzaXRvciBmb3IgY29udHJvbCIsImNvYnJvd3Npbmdfc3RvcF9yZXF1ZXN0IjoiRW5kIHlvdXIgc2NyZWVuc2hhcmluZyBzZXNzaW9uIiwiY29icm93c2luZ19yZXF1ZXN0X2NvbnRyb2xfcmVqZWN0ZWQiOiJZb3VyIHJlcXVlc3Qgd2FzIGRlY2xpbmVkIiwiY29icm93c2luZ19jYW5jZWxfdmlzaXRvcl9tc2ciOiJTY3JlZW5zaGFyaW5nIGlzIGN1cnJlbnRseSB1bmF2YWlsYWJsZSIsImNiX3ZpZXdpbmdfc2NyZWVuX3ZpIjoiQWdlbnQgY2FuIHZpZXcgeW91ciBzY3JlZW4gIiwiY2JfY29udHJvbGxpbmdfc2NyZWVuX3ZpIjoiQWdlbnQgY2FuIGNvbnRyb2wgeW91ciBzY3JlZW4iLCJjYl9naXZlX2NvbnRyb2xfdmkiOiJBbGxvdyBhZ2VudCB0byBjb250cm9sIHlvdXIgc2NyZWVuIiwiY2JfdmlzaXRvcl9zZXNzaW9uX3JlcXVlc3QiOiJDYW4gYWdlbnQgY29udHJvbCB5b3VyIGN1cnJlbnQgc2NyZWVuPyJ9fQ==';</script>

<!-- live chat end -->

<script>

window.onload = function () {
	function resize () {
			var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

			[].forEach.call(document.querySelectorAll('.hero-header'), function (a) {
					a.style.height = h * 0.7 + 'px';
			});
	}
	window.onresize = resize;
	resize();
};

</script>

<script type="text/javascript">
    var trackcmp_email = '';
    var trackcmp = document.createElement("script");
    trackcmp.async = true;
    trackcmp.type = 'text/javascript';
    trackcmp.src = '//trackcmp.net/visit?actid=65978692&e='+encodeURIComponent(trackcmp_email)+'&r='+encodeURIComponent(document.referrer)+'&u='+encodeURIComponent(window.location.href);
    var trackcmp_s = document.getElementsByTagName("script");
    if (trackcmp_s.length) {
        trackcmp_s[0].parentNode.appendChild(trackcmp);
    } else {
        var trackcmp_h = document.getElementsByTagName("head");
        trackcmp_h.length && trackcmp_h[0].appendChild(trackcmp);
    }
</script>

<script type="text/javascript" src="http://www.trksrv45.com/js/78050.js" ></script>
<noscript><img src="http://www.trksrv45.com/78050.png" style="display:none;" /></noscript>

</body>
</html>
