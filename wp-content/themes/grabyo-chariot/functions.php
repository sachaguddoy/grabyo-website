<?php

/* Functions customization
 * functions.php from the parent theme is always loaded
 * This one will always overide the parent's one.
 */
 
 
function grabyo_enqueue_script() {
	wp_enqueue_script( 'ga-events', get_stylesheet_directory_uri().'/js/ga-events.js', ['jquery'], 1.5, true );
	wp_enqueue_script( 'bxslider', get_stylesheet_directory_uri().'/js/jquery.bxslider.js', ['jquery'], 1.5, true );
	wp_enqueue_script( 'main', get_stylesheet_directory_uri().'/js/main.js', ['jquery', 'bxslider'], 1.0, true );
}
add_action( 'wp_enqueue_scripts', 'grabyo_enqueue_script' );


	function print_menu_shortcode($atts, $content = null) {
    		extract(shortcode_atts(array( 'name' => null, ), $atts));
    		return wp_nav_menu( array( 'menu' => $name, 'echo' => false ) );
	}
	add_shortcode('menu', 'print_menu_shortcode');

	function full_features_func ( $atts ) {

		// Query all the category from the custom taxonomy
		$terms = get_terms(array('faq_category'), array('parent' => 0));

		$dir = '/wp-content/themes/grabyo-chariot';

		$output = "";

		foreach ($terms as $term) {

			$output .= "<div class='faq-category-item' id='" . $term->slug . "'>";

				$output .= '<h2 class="faq-category-item-title">' . $term->name . '<span class="toggle-all" data-status="collapsed">[ Expand All ]</span></h2>';

				$termChildren = get_terms(array('faq_category'), array('parent' => $term->term_id));

				if (count($termChildren) == 0) {

					// We display the post of the current category
					$output .= getFeatureCategoryContent($term->slug);

				} else {

					foreach($termChildren as $childTerm) {

						$output .= '<h4 class="faq-subcategory-item-title">' . $childTerm->name . '</h4>';
						$output .= getFeatureCategoryContent($childTerm->slug);
					}
				}

			$output .= "</div>";

		}


		$output .= count($terms) == 0 ? "" : "<script src='" . $dir . "/js/faq.js'></script>";
		return $output;
	}
	add_shortcode ( 'full_features', 'full_features_func');

	function getFeatureCategoryContent($slug) {

		$query = new WP_Query(array(
			'post_type' => 'grabyo_faq',
			'posts_per_page' => 999,
			'tax_query' => array(
				array(
					'taxonomy' => 'faq_category',
					'field'    => 'slug',
					'terms'    => array( $slug ),
					'include_children' => false,
				),
			),
		));

		$output .= "<div class='faq-articles-list'>";

		while($query->have_posts()) : $query->the_post();
			$output .= "<div class='faq-article'>";
				$output .= "<h3 class='faq-article-title'>" . get_the_title() . "</h3>";
				$output .= "<div class='faq-article-text'>" . get_the_content() . "</div>";
			$output .= "</div>";
		endwhile;

		// Reset the query otherwise on next loop iteration it wil break
		wp_reset_query();
		return $output .= "</div>";
	}


	function customers_stories_func( $atts ) {

		$dir = '/wp-content/themes/grabyo-chariot';

		$output  = "<link href='" . $dir . "/css/jquery.bxslider.css'/>";
	  	$output .= "<script src='" . $dir . "/js/jquery.bxslider.js'></script>";
	  	$output .= "<script src='" . $dir . "/js/customers-stories.js'></script>";
	  	return $output;
	}

	add_shortcode( 'customers_stories', 'customers_stories_func' );

	function investors_func( $atts ) {

		$dir = '/wp-content/themes/grabyo-chariot';

		$options = shortcode_atts( array(
			'slug' => '',
			'mobile_slider' => true
		), $atts );

		$output = "";

		/* Load template and do shit here */
	    $args = array(
	  		'posts_per_page' => 4,
		   	'post_type' => 'team',
			'disciplines' => $options['slug']
		);

	  	// Run query
	  	$my_query = new WP_Query($args);

	  	// Append all the scripts and styles
	  	$output .= "<link href='" . $dir . "/css/jquery.bxslider.css'/>";
	  	$output .= "<script src='" . $dir . "/js/jquery.bxslider.js'></script>";
	  	$output .= "<link href='" . $dir . "/css/investors.css'/>";
	  	$output .= "<script src='" . $dir . "/js/investors.js'></script>";
	  	$output .= "<link src='" . $dir . "/style.css'>";

	  	// Create the grid
	  	$output .= "<div class='investor-grid row'><div class='slider'>";

	  	// Iterate over all the team post
	  	while($my_query->have_posts()) : $my_query->the_post();

	  		$output .= "<div class='col-md-3 col-sm-3 col-xs-3'>";
	  		$output .= "<div class='investor-image-box'>";

	  			$img_thumb = wp_get_attachment_image_src( get_post_thumbnail_id(), array(335,335));
	  			// Append that image in a beautiful span
	  			$output .= "<div class='text-center'><img class='img-responsive' style='max-width: 100%;' src='" . $img_thumb[0] . "'/>";
	  	  		$output .= "<h3 class='text-center'>" . get_the_title() . '</h3>';
	  	  		$output .= "<p class='social'>" . get_the_content() . "</p>";


	  	  		$output .= "</div>";
	  		$output .= "</div>";
	  		$output .= "</div>";

	  	endwhile;
	  	$output .= "</div></div>";

	  	wp_reset_query();

		return $output;
	}

	add_shortcode( 'investors', 'investors_func' );

	function custom_video_function ( $atts ) {

		$options = shortcode_atts( array(
			'title' => '',
			'img' => '',
			'mp4' => '',
			'ogg' => '',
			'webm' => '',
			'error_msg' => 'Your browser does not support the video tag.',
		), $atts );

		$output = "<section class='investor-player' id='hear-what-the-players-say'>";

			$output .= "<div class='investor-player-image'>";

				$output .= "<div class='img-caption'>";
					$output .= "<h2>" . $options['title'] . "</h2>";
				$output .= "</div>";

				$output .= "<i class='font-icon-play' id='investor-video-displayer'></i>";

			$output .= "</div>";

			$output .= "<video controls class='investors-video' id='investors-video' width='100%' height='100%' style='max-width:1100px; display:none; margin:0 auto;'>";

				if (strlen($options['mp4']) > 0) {
					$output .= "<source src='" . $options['mp4'] . "' type='video/mp4'/>";
				}
				if (strlen($options['ogg']) > 0) {
					$output .= "<source src='" . $options['ogg'] . "' type='video/ogg'/>";
				}
				if (strlen($options['webm']) > 0) {
					$output .= "<source src='" . $options['webm'] . "' type='video/webm'/>";
				}

				$output .= $options['error_msg'];

			$output .= '</video>';

		$output .= '</section>';

		return $output;
	};

	add_shortcode( 'home_video', 'custom_video_function' );

	add_filter('show_admin_bar', '__return_false');

	add_action( 'init', 'create_faq_post_type' );
	add_action( 'init', 'create_faq_taxonomy' );

	function create_faq_taxonomy() {
 		register_taxonomy( 'faq_category', 'grabyo_faq', array(
	 			'hierarchical' => true,
	 			'label' => 'FAQ Categories',
	 			'query_var' => true,
	 			'rewrite' => true
 			)
 		);
	}

	function create_faq_post_type() {
	  register_post_type( 'grabyo_faq',
	    array(
	      'labels' => array(
	        'name' => __( 'FAQ' ),
	        'singular_name' => __( 'Item' )
	      ),
	      'taxonomies' => array('faq_category'),
	      'supports'   => array('title', 'editor'),
	      'public' => true,
	      'has_archive' => true,
	    )
	  );
	}

    add_filter('rss2_ns', 'rss_namespace');

    function rss_namespace() {
        echo 'xmlns:webfeeds="http://webfeeds.org/rss/1.0" ';
				echo 'xmlns:media="http://search.yahoo.com/mrss/"';
    }

    add_filter('rss2_head', 'rss_analytics');
    function rss_analytics() {
        echo '<webfeeds:analytics id="UA-39122932-2" engine="GoogleAnalytics"/>';
        echo '<webfeeds:logo>http://tu8kxuf6c74ck7od4f4h02cm.wpengine.netdna-cdn.com/wp-content/themes/grabyo-chariot/img/logo_header.png</webfeeds:logo>';
        echo '<webfeeds:accentColor>F03F55</webfeeds:accentColor>';
        echo '<webfeeds:cover image="http://about.grabyo.com/wp-content/uploads/2014/11/team_hashtag_.png" />';
        echo '<webfeeds:icon>http://about.grabyo.com/wp-content/uploads/2014/02/logo_.png</webfeeds:icon>';
    }

    // add_filter('the_excerpt_rss', 'rss_feature_image');
    // add_filter('the_content_feed', 'rss_feature_image');
    function rss_feature_image($content) {
        global $post;
        if ( has_post_thumbnail( $post->ID ) ){
            $content = '<div>' . get_the_post_thumbnail( $post->ID, 'large', array( 'style' => 'margin-bottom: 15px; max-width: 100%;', 'class' => 'webfeedsFeaturedVisual', ) ) . '</div>' . $content;
        }
        return $content;
    }

		add_action('rss2_item', 'add_feature_image');

		function add_feature_image() {
			global $post;
			if(has_post_thumbnail($post->ID)):
				$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), ‘medium’);

				echo '<media:content url="' . $large_image_url[0] . '" medium="image"/>';

			endif;
		}

		// Single Posts Header Settings
	    function az_post_header2($postid) {

				global $options;
				global $post;

				$check_page_settings = get_post_meta($postid, '_az_header_settings', true);

		    	$bg = get_post_meta($postid, '_az_header_bg', true);
				$bg_overlay = get_post_meta($postid, '_az_header_overlay', true);
				$bgattachment = get_post_meta($postid, '_az_header_bg_attachment', true);
				$page_title = get_post_meta($postid, '_az_page_title', true);
				$page_caption = get_post_meta($postid, '_az_page_caption', true);
				$page_align_text = get_post_meta($postid, '_az_page_text_align', true);
				$rev_slider_alias = get_post_meta($postid, '_az_intro_slider_header', true);

			?>

		    <?php if ( $check_page_settings == "enabled") { ?>

		    <?php if( !empty($bg) ) { ?>
		        <section id="image-static">
		        <?php if(!empty($bg_overlay) && $bg_overlay == 'on') { echo '<span class="overlay-bg"></span>'; } ?>
				<div class="fullimage-container titlize" style="background: url('<?php echo $bg; ?>') center center no-repeat; background-attachment: <?php echo $bgattachment; ?>; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">

		            <div class="container pagize">
		            	<div class="row">
		                	<div class="col-md-12 <?php echo $page_align_text; ?>">
								 <?php if( !empty($page_title) ) { ?>
		                        	<h2><?php echo $page_title; ?></h2>
		                        <?php } else { ?>
		                        	<h2><?php echo the_title(); ?></h2>
		                        <?php } ?>

		                        <?php if( !empty($page_caption) ) { ?>
		                        	<p class="page-caption"><?php echo $page_caption; ?></p>
		                        <?php } else { ?>
		                            <div class="entry-meta entry-header">
		                                <span class="published"><?php the_time( get_option('date_format') ); ?></span>
		                            </div>
		                        <?php } ?>
		            		</div>
		            	</div>
		            </div>

		        </div>
		        </section>
			    <?php } else if( !empty($rev_slider_alias) ) { ?>
		        <section id="slider-header">
		            <?php echo do_shortcode('[rev_slider '.$rev_slider_alias.']'); ?>
		        </section>
				<?php } else { ?>
		        <section id="title-page">
		        	<span class="overlay-bg-fill"></span>
		        	<div class="container pagize">
		            	<div class="row">
		                	<div class="col-md-12 <?php echo $page_align_text; ?>">
								 <?php if( !empty($page_title) ) { ?>
		                        	<h2><?php echo $page_title; ?></h2>
		                        <?php } else { ?>
		                        	<h2><?php echo the_title(); ?></h2>
		                        <?php } ?>

		                        <?php if( !empty($page_caption) ) { ?>
		                        	<p class="page-caption"><?php echo $page_caption; ?></p>
		                        <?php } else { ?>
		                            <div class="entry-meta entry-header">
		                                <span class="published"><?php the_time( get_option('date_format') ); ?></span>
		                            </div>
		                        <?php } ?>
		            		</div>
		            	</div>
		            </div>
		        </section>
				<?php } ?>

		    <?php }
		    }

?>
