<?php
/*
Template Name: bxslider
*/
?>

<?php get_header(); ?>

<?php az_page_header($post->ID); ?>

<div id="content">



	<?php /* Now come the content */ ?>

	<section class="content-wrapper">
		<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
	        <?php //edit_post_link( __('Edit', AZ_THEME_NAME), '<span class="edit-post">[', ']</span>' ); ?>
	        <?php the_content(); ?>
	        <?php wp_link_pages(array('before' => '<p><strong>'.__('Pages:', 'zilla').'</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
	    <?php endwhile; endif; ?>
	</section>
</div>

<!-- Slider mac -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script type="text/javascript" src="/wp-content/themes/grabyo-chariot/js/jquery.bxslider.js"></script>

<!-- Slider mac end -->

<script>

</script>

<?php get_footer(); ?>
