<?php $options = get_option('chariot');

$animation_options = null;
if( !empty($options['enable-animation']) && $options['enable-animation'] == 1) {
	$animation_options = 'animation-enabled';
} else {
	$animation_options = 'no-animation';
}

$animation_effects_options = null;
if( !empty($options['enable-animation-effects']) && $options['enable-animation-effects'] == 1) {
	$animation_effects_options = 'animation-effects-enabled';
} else {
	$animation_effects_options = 'no-animation-effects';
}

?>
<!DOCTYPE html>
<!--[if gte IE 9]>
<html class="no-js lt-ie9 animated-content no-animation no-animation-effects" lang="en-US">
<![endif]-->
<html <?php language_attributes(); ?> class="<?php echo $animation_options; ?> <?php echo $animation_effects_options; ?>">
<head>

<div style="display:none">
<!-- Google Code for Remarketing Tag -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 873702127;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/873702127/?guid=ON&amp;script=0"/>
</div>
</noscript>
</div>

<!-- Freshsales.io for Web tag code -->
<script> function createTempFunction(function_name) { (window.freshsales)[function_name]=function() { (window.freshsales).push([function_name].concat(Array.prototype.slice.call(arguments,0))) }; } (function(url, app_token) { window.assets_url='//d36igh5hsho8rx.cloudfront.net'; window.freshsales = window.freshsales || []; functions_list='init identify trackPageView trackEvent set'.split(' '); for(var i=0; i < functions_list.length; i++) { var function_name = functions_list[i]; createTempFunction(function_name); } var script_tag = document.createElement('script'); script_tag.async=1; script_tag.src= window.assets_url +'/assets/analytics.js'; var first_script_tag = document.getElementsByTagName('script')[0]; first_script_tag.parentNode.insertBefore(script_tag,first_script_tag); freshsales.init(url, app_token); })("https://grabyo.freshsales.io","f7119ef21f7f9441d467220282d3b4bd"); </script>
<!-- Freshsales.io for Web tag code end -->

<!-- linkedin universal website tag code -->
	<script type="text/javascript">
_linkedin_data_partner_id = "13851";
</script><script type="text/javascript">
(function(){var s = document.getElementsByTagName("script")[0];
var b = document.createElement("script");
b.type = "text/javascript";b.async = true;
b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
s.parentNode.insertBefore(b, s);})();
</script>
<!-- linkedin universal website tag code end -->

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1020071118111439');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1020071118111439&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

<!-- linkedin -->
<script type="text/javascript">
_linkedin_data_partner_id = "29058";
</script><script type="text/javascript">
(function(){var s = document.getElementsByTagName("script")[0];
var b = document.createElement("script");
b.type = "text/javascript";b.async = true;
b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
s.parentNode.insertBefore(b, s);})();
</script>
<!-- linkedin -->

	<!-- Twitter universal website tag code -->
<script>
!function(e,n,u,a){e.twq||(a=e.twq=function(){a.exe?a.exe.apply(a,arguments):
a.queue.push(arguments);},a.version='1',a.queue=[],t=n.createElement(u),
t.async=!0,t.src='//static.ads-twitter.com/uwt.js',s=n.getElementsByTagName(u)[0],
s.parentNode.insertBefore(t,s))}(window,document,'script');
// Insert Twitter Pixel ID and Standard Event data below
twq('init','nvcet');
twq('track','PageView');
</script>
<!-- End Twitter universal website tag code -->
<!-- linkedin -->
<script type="text/javascript"> _linkedin_data_partner_id = "29058"; </script><script type="text/javascript"> (function(){var s = document.getElementsByTagName("script")[0]; var b = document.createElement("script"); b.type = "text/javascript";b.async = true; b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js"; s.parentNode.insertBefore(b, s);})(); </script>
<!-- linkedin end -->

<!-- Meta Tags -->
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<!-- Mobile Specifics -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Mobile Internet Explorer ClearType Technology -->
<!--[if IEMobile]>  <meta http-equiv="cleartype" content="on">  <![endif]-->

<?php

if( !empty($options['use-logo'])) {
	$logo = $options['logo'];
	$retina_logo = $options['retina-logo'];
	$width = $options['logo-size-width'];
	$height = $options['logo-size-height'];

	if ($retina_logo == "") {
		$retina_logo = $logo;
	}
}

?>

<?php if(!empty($options['favicon'])) { ?>
<!--Shortcut icon-->
<link rel="shortcut icon" href="<?php echo $options['favicon']?>" />



<?php } ?>

<!-- Title -->
<title><?php wp_title('|',true,'right'); ?><?php bloginfo('name'); ?></title>

<!-- RSS & Pingbacks -->
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo( 'name' ); ?> RSS Feed" href="<?php bloginfo( 'rss2_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

<!-- Facebook SDK Loader -->
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=493276274073142";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
<!-- end Facebook SDK Loader -->
	
<!-- Start Wrap All -->
<?php
$boxed = null;
if(!empty($options['enable-boxed-layout']) && $options['enable-boxed-layout'] == '1') {
    $boxed = ' boxed';
}
?>
<div class="wrap_all<?php echo $boxed; ?>">

<!-- Header -->
<?php

$header_class = null;
$main_class = null;
if( !empty($options['enable-sticky-header']) && $options['enable-sticky-header'] == 1) {
    $header_class = 'sticky-header';
    $main_class = 'sticky-header-enabled';
} else {
    $header_class = 'normal-header';
    $main_class = 'normal-header-enabled';
}

?>
<header class="<?php echo $header_class; ?>">
	<div class="container">
    	<div class="row">

		<div class="col-md-2">
		<div id="logo">
			<a href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>">
				<?php
				if( !empty($options['use-logo'])) { ?>
					<img class="standard" src="<?php echo $logo; ?>" alt="<?php bloginfo('name'); ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
					<img class="retina" src="<?php echo $retina_logo; ?>" alt="<?php bloginfo('name'); ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
				<?php } else { ?>
					<?php bloginfo('name'); ?>
				<?php } ?>
			</a>
		</div>
		</div>

        <div class="col-md-10">
            <!-- Mobile Menu -->
            <a id="mobile-nav" class="menu-nav" href="#menu-nav"><span class="menu-icon"></span></a>

            <!-- Standard Menu -->
            <div id="menu">
                <ul id="menu-nav">
                <?php
                    if(has_nav_menu('primary_menu')) {
						wp_nav_menu( array('theme_location' => 'primary_menu', 'menu' => 'Primary Menu', 'container' => '', 'items_wrap' => '%3$s' ) );
                    }
                    else {
						echo '<li><a href="#">No menu assigned!</a></li>';
                    }
                ?>
                </ul>
            </div>
        </div>

        </div>
    </div>
</header>
<!-- End Header -->

<!-- Mobile Navigation Mobile Menu -->
<div id="navigation-mobile">
	<div class="container">
    	<div class="row">
        	<div class="col-md-12">
            	<ul id="menu-nav-mobile">
                <?php
                    if(has_nav_menu('primary_menu')) {
						wp_nav_menu( array('theme_location' => 'primary_menu', 'menu' => 'Primary Menu', 'container' => '', 'items_wrap' => '%3$s' ) );
                    }
                    else {
						echo '<li><a href="#">No menu assigned!</a></li>';
                    }
                ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End Navigation Mobile Menu -->

<div class="chat-widget" id="chat-widget">

	<i class="fa-comments"></i>

	<span class="light">Chat with us</span>

</div>

<!-- Start Main -->
<div id="main" class="<?php echo $main_class; ?>">
